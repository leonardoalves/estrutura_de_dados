//TAD - fila Circular de bebidas .c

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cbebida.h"

int main (){
	
	Fila bebida ;
	
	fIniciar (&bebida);
	fInsere(&bebida,"Vinho","Sao Braz",5);
	fInsere(&bebida,"Cachaca","Ypioca",12 );
	fInsere(&bebida,"Vodka","Belvedere",125);
	fInsere(&bebida,"Cerveja","Itaipava",7.50);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	

}

