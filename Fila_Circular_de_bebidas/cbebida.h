//TAD - Fila Circula de bebidas .h

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define MAX 5

struct bebida{
	char tipo[30];
	char marca[30];
	float preco ;
};
typedef struct bebida Bebida;

struct fila {
	Bebida fila [MAX];
	int qtelemento;
	int inicio,fim;
};
typedef struct fila Fila ;

void fIniciar (Fila *f){
	printf("iniciar fila:\n");
	
	f-> inicio = 0;
	f-> fim =-1;
	f-> qtelemento=0;
	
}

void fInsere (Fila *f , char tipo[], char marca[], float preco){
	
	printf("Incerir na fila :");
	
	Bebida b ;
	
	strcpy (b.tipo,tipo);
	strcpy (b.marca,marca);
	b.preco = preco;
	
	if (f->qtelemento == MAX){
		printf ("fila esta cheia.\n");
		return;
	}
	
	if(f->fim == MAX-1){
		f->fim = -1;
	}
	
	f->fim++;
	f->fila[f->fim]=b;
	f->qtelemento++;
}

void fRemove (Fila *f){
	
	if(f->inicio == MAX){
		f->inicio=0;
	}
	
	printf("remover da fila.\n ");
	
	f->inicio++;
	f->qtelemento--;
	
}

void fImprime (Fila *f){
	
	printf ("\n\n\n   FILA  \n");
	
	if (f->qtelemento==0){
		printf ("Fila Vazia");
	}
	else {
		printf ("Tipo de bebida : %s\n",f->fila[f->inicio].tipo);
		printf ("Marca da bebida : %s\n",f->fila[f->inicio].marca);
		printf ("Preco da Garrafa : %f\n",f->fila[f->inicio].preco);
	}
}

