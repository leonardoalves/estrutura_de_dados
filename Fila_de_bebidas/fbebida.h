//TAD - Fila de bebidas .h

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define MAX 5

struct bebida{
	char tipo[30];
	char marca[30];
	float preco ;
};
typedef struct bebida Bebida;

struct fila {
	
	Bebida fila[10];
	int inicio , fim;
	
};
typedef struct fila Fila; 

void fIniciar (Fila *f){
	
	printf ("iniciar fila : \n");
	
	f->inicio = 0;
	f->fim - 1;
	
}

void fInsere (Fila *f, char tipo[], char marca[], float preco ){
	
	Bebida b ;
	
	strcpy (b.tipo,tipo);
	strcpy (b.marca,marca);
	b.preco = preco;
	
	if (f->fim == MAX -1){
		printf("Fila cheia !\n");
	}
	else{
		f->fim++;
		f->fila[f->fim]=b;
	}
	
} 

void fRemove (Fila *f){
	
	printf ("remover da fila :\n");
	
	f->inicio ++;
	
}

void fImprime (Fila *f){	
		
		printf ("\n\n\n   FILA  \n");
		printf ("Tipo de bebida : %s\n",f->fila[f->inicio].tipo);
		printf ("Marca da bebida : %s\n",f->fila[f->inicio].marca);
		printf ("Preco da Garrafa : %f\n",f->fila[f->inicio].preco);
	
}
