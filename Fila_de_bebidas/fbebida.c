//TAD - Fila de inteiros .h

//TAD - fila de bebidas .c

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fbebida.h"

int main(){
	
	Fila bebida ;
	
	fIniciar (&bebida);
	fInsere(&bebida,"Vinho","Sao Braz",5);
	fInsere(&bebida,"Cachaca","Ypioca",12 );
	fInsere(&bebida,"Vodka","Belvedere",125);
	fInsere(&bebida,"Cerveja","Itaipava",7.50);
	fRemove(&bebida);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	fRemove(&bebida);
	fImprime(&bebida);
	
}

