//TAD - Pilha estatica de bebidas .h`-`

#include <string.h>
#include <stdio.h>
#include <stdlib.h> 
#define MAX 5

typedef struct bebida {
	char tipo[30];
	char marca[30];
	float preco ;
}Bebida;
 

struct pilha{
	Bebida pilha[MAX];
	int topo;
};
typedef struct pilha Pilha ;

void pIniciar (Pilha *p){
	
	p->topo = -1;
	
}

void pInsere (Pilha *p, char tipo[], char marca[], float preco){
	
	Bebida novaBebida ;
	
	strcpy (novaBebida.tipo,tipo);
	strcpy (novaBebida.marca,marca);
	novaBebida.preco = preco;
	
	if (p->topo == MAX-1){
		printf("Erro ! Pilha esta cheia ./n");
	}
	else{
		p->topo = p -> topo + 1;
		p->pilha [p->topo] = novaBebida;
		
	} 
}

void pRemove (Pilha *p){
	if (p->topo == 0){
		("pilha es vazia. \n");
	}
	else {
		
		p->topo--;
		printf ("topo removido. \n");
	}
	
}

void pExibe (Pilha *p){
	printf("\n\n\n  TOPO DA PILHA  \n");
	printf("Tipo da bebida : %s\n",p->pilha[p->topo].tipo);
	printf("Marca da bebida : %s\n",p->pilha[p->topo].marca);
	printf("Preco: %f\n",p->pilha[p->topo].preco);
	
}
