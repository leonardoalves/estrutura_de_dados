//TAD - Pilha estatica de bebidas .c

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "pbebida.h"

int main (){
	
	Pilha bebida ;
	
	pIniciar (&bebida);
	pInsere (&bebida,"Vinho","Sao Braz",5 );
	pInsere (&bebida,"Cachaca","Ypioca",12 );
	pInsere (&bebida,"Vodka","Belvedere",125 );
	pInsere (&bebida,"Cerveja","Itaipava",2.50 );
	pExibe (&bebida);
	pRemove (&bebida);
	pExibe (&bebida);
	pRemove (&bebida);
	pExibe (&bebida);
	pRemove (&bebida);
	pExibe (&bebida);
	pRemove (&bebida);
	


}
