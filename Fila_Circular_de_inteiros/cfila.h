//TAD - fila circular de inteiros .h

#include <stdio.h>
#include <stdlib.h>
#define MAX 10

struct fila {
	int fila [MAX];
	int qtelemento;
	int inicio,fim;
};
typedef struct fila Fila ;

void fIniciar (Fila *f){
	printf("iniciar fila:\n");
	
	f-> inicio = 0;
	f-> fim =-1;
	f-> qtelemento=0;
	
}

void fInsere (Fila *f , int x){
	
	if (f->qtelemento == MAX){
		printf ("fila esta cheia.\n");
		return;
	}
	
	if(f->fim == MAX-1){
		f->fim = -1;
	}
	
	f->fim++;
	f->fila[f->fim]=x;
	f->qtelemento++;
}

void fRemove (Fila *f){
	
	if(f->inicio == MAX){
		f->inicio=0;
	}
	
	printf("remover da fila.\n ");
	
	f->inicio++;
	f->qtelemento--;
	
}

void fImprime (Fila *f){
	
	printf ("FILA : %d\n",f->fila[f->inicio]);
	
}
